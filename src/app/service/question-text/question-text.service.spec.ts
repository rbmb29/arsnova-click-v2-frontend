import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CustomMarkdownService } from '../custom-markdown/custom-markdown.service';
import { CustomMarkdownServiceMock } from '../custom-markdown/CustomMarkdownServiceMock';
import { FooterBarService } from '../footer-bar/footer-bar.service';
import { SettingsMockService } from '../settings/settings.mock.service';
import { SettingsService } from '../settings/settings.service';
import { QuestionTextService } from './question-text.service';

describe('QuestionTextService', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpClientTestingModule,
      ],
      providers: [
        {
          provide: CustomMarkdownService,
          useClass: CustomMarkdownServiceMock,
        }, QuestionTextService,
        {
          provide: SettingsService,
          useClass: SettingsMockService,
        },
        {
          provide: FooterBarService,
          useValue: {},
        }
      ],
    });
  }));

  it('should be created', waitForAsync(inject([QuestionTextService], (service: QuestionTextService) => {
    expect(service).toBeTruthy();
  })));
});
