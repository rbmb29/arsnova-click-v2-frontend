import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PLATFORM_ID } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { jwtOptionsFactory } from '../../../lib/jwt.factory';
import { I18nTestingModule } from '../../../shared/testing/i18n-testing/i18n-testing.module';

import { AdminSettingsApiService } from './admin-settings-api.service';

describe('AdminSettingsService', () => {
  let service: AdminSettingsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [I18nTestingModule, HttpClientTestingModule, JwtModule.forRoot({
        jwtOptionsProvider: {
          provide: JWT_OPTIONS,
          useFactory: jwtOptionsFactory,
          deps: [PLATFORM_ID],
        },
      })
      ],
    });
    service = TestBed.inject(AdminSettingsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
