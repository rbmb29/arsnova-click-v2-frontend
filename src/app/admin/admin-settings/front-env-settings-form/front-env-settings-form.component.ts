import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { themes } from '../../../lib/available-themes';
import { LoginMechanism, Title } from '../../../lib/enums/enums';
import { ISettings } from '../../../lib/settings/ISettings';
import { AdminApiService } from '../../../service/api/admin/admin-api.service';
import { AdminSettingsApiService } from '../../../service/api/admin/admin-settings-api.service';
import { SettingsService } from '../../../service/settings/settings.service';

@Component({
  selector: 'app-front-env-settings-form',
  templateUrl: './front-env-settings-form.component.html',
  styleUrls: ['./front-env-settings-form.component.scss'],
})
export class FrontEnvSettingsFormComponent implements OnInit {
  public settingsForm: FormGroup;
  public settings: ISettings;
  public isSending = false;
  public readonly titles: Array<string> = Object.values(Title);
  public readonly leaderBoardAlgorithms: Array<string> = ['PointBased', 'TimeBased'];
  public readonly loginMechanismKeyStrings: Array<string> = Object.values(LoginMechanism);
  public readonly availableThemesKeyStrings: Array<string> = themes.map(t => t.id);

  constructor(
    private fb: FormBuilder,
    private adminApiService: AdminApiService,
    private settingsService: SettingsService,
    private adminSettingsApi: AdminSettingsApiService,
  ) {
  }

  public ngOnInit(): void {
    this.adminSettingsApi.getSettings('front_env').subscribe((settings) => {
      if (!settings) {
        console.log('no settings received');
      }

      this.settings = settings;
      this.settingsForm = this.fb.group({
        title: [
          settings.config.title,
          [],
        ],
        appName: [
          settings.config.appName,
          [],
        ],
        backendVersion: [
          settings.config.backendVersion,
          [],
        ],
        sentryDSN: [
          settings.config.sentryDSN,
          [],
        ],
        leaderboardAlgorithm: [
          settings.config.leaderboardAlgorithm,
          [],
        ],
        leaderboardAmount: [
          settings.config.leaderboardAmount,
          [],
        ],
        enableBonusToken: [
          settings.config.enableBonusToken,
          [],
        ],
        readingConfirmationEnabled: [
          settings.config.readingConfirmationEnabled,
          [],
        ],
        confidenceSliderEnabled: [
          settings.config.confidenceSliderEnabled,
          [],
        ],
        infoAboutTabEnabled: [
          settings.config.infoAboutTabEnabled,
          [],
        ],
        infoProjectTabEnabled: [
          settings.config.infoProjectTabEnabled,
          [],
        ],
        infoBackendApiEnabled: [
          settings.config.infoBackendApiEnabled,
          [],
        ],
        requireLoginToCreateQuiz: [
          settings.config.requireLoginToCreateQuiz,
          [],
        ],
        forceQuizTheme: [
          settings.config.forceQuizTheme,
          [],
        ],
        loginMechanism: [
          settings.config.loginMechanism,
          [],
        ],
        showLoginButton: [
          settings.config.showLoginButton,
          [],
        ],
        showJoinableQuizzes: [
          settings.config.showJoinableQuizzes,
          [],
        ],
        showPublicQuizzes: [
          settings.config.showPublicQuizzes,
          [],
        ],
        persistQuizzes: [
          settings.config.persistQuizzes,
          [],
        ],
        availableQuizThemes: [
          settings.config.availableQuizThemes,
          [],
        ],
        defaultTheme: [settings.config.defaultTheme, []],
        darkModeCheckEnabled: [
          settings.config.darkModeCheckEnabled,
          [],
        ],
        enableQuizPool: [
          settings.config.enableQuizPool,
          [],
        ],
        showInfoButtonsInFooter: [
          settings.config.showInfoButtonsInFooter,
          [],
        ],
        vapidPublicKey: [
          settings.config.vapidPublicKey,
          [],
        ],
        markdownFilePostfix: [
          settings.config.markdownFilePostfix,
          [],
        ],
        loginButtonLabelConfiguration: [settings.config.loginButtonLabelConfiguration, []],
        twitterEnabled: [settings.config.twitterEnabled, []],
        cacheQuizAssets: [settings.config.cacheQuizAssets, []],
        createQuizPasswordRequired: [settings.config.createQuizPasswordRequired, []],
        limitActiveQuizzes: [settings.config.limitActiveQuizzes, []],
      });
    });
  }

  public async submitForm(): Promise<void> {
    this.isSending = true;
    for (const control of Object.values(this.settingsForm.controls)) {
      control.markAsDirty();
      control.updateValueAndValidity();
      if (control.invalid) {
        console.log(control);
      }
    }

    if (this.settingsForm.valid) {
      this.settings.config = { ...this.settings.config, ...this.settingsForm.getRawValue() };
      this.adminApiService.editSettings(this.settings).subscribe((res) => {
        if (res) {
          this.settingsService.frontEnv = res.config;
        } else {
          console.log('error udpating settings');
        }
        this.isSending = false;
      });
    }
    this.isSending = false;
  }
}
