import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RxStompService } from '@stomp/ng2-stompjs';
import { MarkdownService } from 'ngx-markdown';
import { FooterBarService } from '../../service/footer-bar/footer-bar.service';
import { QuizService } from '../../service/quiz/quiz.service';
import { UserService } from '../../service/user/user.service';
import { I18nTestingModule } from '../../shared/testing/i18n-testing/i18n-testing.module';
import { AdminSettingsComponent } from './admin-settings.component';

describe('AdminSettingsComponent', () => {
  let component: AdminSettingsComponent;
  let fixture: ComponentFixture<AdminSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, I18nTestingModule, ReactiveFormsModule],
        declarations: [AdminSettingsComponent],
        providers: [
          FormBuilder,
          {
            provide: UserService,
            useValue: {
              isAuthorizedFor: () => true,
            },
          },
          JwtHelperService,
          {
            provide: MarkdownService,
            useValue: {},
          },
          {
            provide: QuizService,
            useValue: {
              loadDataToEdit: () => new Promise<void>(resolve => resolve()),
            },
          },
          {
            provide: FooterBarService,
            useValue: {
              replaceFooterElements: () => {},
            },
          },
          DatePipe,
          RxStompService,
        ],
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
