import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PLATFORM_ID } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SwUpdate } from '@angular/service-worker';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FontAwesomeTestingModule } from '@fortawesome/angular-fontawesome/testing';
import { NgbModalModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { RxStompService } from '@stomp/ng2-stompjs';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HotkeysService } from 'angular2-hotkeys';
import { TOAST_CONFIG } from 'ngx-toastr';
import { of } from 'rxjs';
import { SwUpdateMock } from '../../../../../../_mocks/_services/SwUpdateMock';
import { HeaderComponent } from '../../../../../header/header/header.component';
import { SurveyQuestionEntity } from '../../../../../lib/entities/question/SurveyQuestionEntity';
import { jwtOptionsFactory } from '../../../../../lib/jwt.factory';
import { LivePreviewComponent } from '../../../../../live-preview/live-preview/live-preview.component';
import { ConnectionMockService } from '../../../../../service/connection/connection.mock.service';
import { ConnectionService } from '../../../../../service/connection/connection.service';
import { CustomMarkdownService } from '../../../../../service/custom-markdown/custom-markdown.service';
import { CustomMarkdownServiceMock } from '../../../../../service/custom-markdown/CustomMarkdownServiceMock';
import { FooterBarService } from '../../../../../service/footer-bar/footer-bar.service';
import { HeaderLabelService } from '../../../../../service/header-label/header-label.service';
import { QuestionTextService } from '../../../../../service/question-text/question-text.service';
import { QuizMockService } from '../../../../../service/quiz/quiz-mock.service';
import { QuizService } from '../../../../../service/quiz/quiz.service';
import { SettingsMockService } from '../../../../../service/settings/settings.mock.service';
import { SettingsService } from '../../../../../service/settings/settings.service';
import { SharedService } from '../../../../../service/shared/shared.service';
import { ThemesMockService } from '../../../../../service/themes/themes.mock.service';
import { ThemesService } from '../../../../../service/themes/themes.service';
import { TrackingMockService } from '../../../../../service/tracking/tracking.mock.service';
import { TrackingService } from '../../../../../service/tracking/tracking.service';
import { I18nTestingModule } from '../../../../../shared/testing/i18n-testing/i18n-testing.module';
import { AnsweroptionsDefaultComponent } from './answeroptions-default.component';

describe('AnsweroptionsDefaultComponent', () => {
  let component: AnsweroptionsDefaultComponent;
  let fixture: ComponentFixture<AnsweroptionsDefaultComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        I18nTestingModule,
        RouterTestingModule,
        NgbModalModule,
        AngularSvgIconModule.forRoot(),
        NgbPopoverModule,
        FontAwesomeModule,
        HttpClientTestingModule, JwtModule.forRoot({
          jwtOptionsProvider: {
            provide: JWT_OPTIONS,
            useFactory: jwtOptionsFactory,
            deps: [PLATFORM_ID],
          },
        }),
        FontAwesomeTestingModule,
      ],
      providers: [
        RxStompService,
        {
          provide: CustomMarkdownService,
          useClass: CustomMarkdownServiceMock,
        }, {
          provide: QuizService,
          useValue: {
            quizUpdateEmitter: of(null),
            loadDataToEdit: () => new Promise<void>(resolve => resolve()),
            quiz: {
              currentQuestionIndex: 0,
              questionList: [
                {
                  isValid: () => true,
                  tags: [],
                  removeAnswerOption: () => {},
                  addDefaultAnswerOption: () => {},
                  answerOptionList: [
                    {
                      TYPE: 'DefaultAnswerOption',
                      answerText: '',
                    }
                  ],
                }
              ],
            },
          },
        }, {
          provide: ThemesService,
          useClass: ThemesMockService
        }, {
          provide: FooterBarService,
          useValue: {
            footerElemBack: {
              onClickCallback: () => {},
              restoreClickCallback : () => {},
              isActive: true,
            },
            footerElemSaveQuiz: {
              onClickCallback: () => {},
              restoreClickCallback : () => {},
              isActive: true,
            }
          },
        }, {
          provide: SettingsService,
          useClass: SettingsMockService,
        }, {
          provide: ConnectionService,
          useClass: ConnectionMockService,
        }, SharedService, HeaderLabelService, QuestionTextService, {
          provide: TrackingService,
          useClass: TrackingMockService,
        }, {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({
              get: () => 0,
            }),
            queryParamMap: of({
              get: () => null,
            }),
          },
        }, {
          provide: SwUpdate,
          useClass: SwUpdateMock,
        }, {
          provide: TOAST_CONFIG,
          useValue: {
            default: {},
            config: {},
          },
        }, {
          provide: HotkeysService,
          useValue: {}
        },
      ],
      declarations: [
        HeaderComponent, LivePreviewComponent, AnsweroptionsDefaultComponent,
      ],
    }).compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(AnsweroptionsDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should be created', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should contain a TYPE reference', waitForAsync(() => {
    expect(AnsweroptionsDefaultComponent.TYPE).toEqual('AnsweroptionsDefaultComponent');
  }));

  it('should add an answer', () => {
    component.addAnswer();
    expect(component.question.answerOptionList.length).toEqual(1);
    expect(component.question.answerOptionList[0].TYPE).toEqual('DefaultAnswerOption');
  });

  it('should update the answertext', () => {
    const value = 'newValue';
    const event = <any>{ target: { value } };
    component.addAnswer();
    component.updateAnswerValue(event, 0);
    expect(component.question.answerOptionList[0].answerText).toEqual(value);
  });

  it('should toggle the showOneAnswerPerRow option of the question', () => {
    const initValue = component.question.showOneAnswerPerRow;
    component.toggleShowOneAnswerPerRow();
    expect(component.question.showOneAnswerPerRow).not.toEqual(initValue);
  });

  it('should toggle the displayAnswerText option of the question', () => {
    const initValue = component.question.displayAnswerText;
    component.toggleShowAnswerContentOnButtons();
    expect(component.question.displayAnswerText).not.toEqual(initValue);
  });

  it('should toggle the multipleSelectionEnabled option of a survey question', () => {
    const initValue = (
      <SurveyQuestionEntity>component.question
    ).multipleSelectionEnabled;
    component.toggleMultipleSelectionSurvey();
    expect((
      <SurveyQuestionEntity>component.question
    ).multipleSelectionEnabled).not.toEqual(initValue);
  });
});
