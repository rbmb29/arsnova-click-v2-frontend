export interface IMotdEntity {
    _id?: String;
    mid?: number;
    title: string;
    isPinned: boolean;
    content: string;
    createdAt?: Date;
    updatedAt?: Date;
    expireAt: Date;
}
