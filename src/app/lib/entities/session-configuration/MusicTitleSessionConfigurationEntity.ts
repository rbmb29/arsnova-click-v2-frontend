export class MusicTitleSessionConfigurationEntity {
  public lobby: string;
  public countdownRunning: string;
  public countdownEnd: string;

  constructor(settingsService, props) {
    this.lobby = props.lobby ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.titleConfig.lobby;
    this.countdownRunning = props.countdownRunning ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.titleConfig.countdownRunning;
    this.countdownEnd = props.countdownEnd ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.titleConfig.countdownEnd;
  }
}
