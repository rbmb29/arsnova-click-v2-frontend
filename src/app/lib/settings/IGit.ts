export interface IGit {
    frontendGitlabId: number;
    backendGitlabId: number;
    gitlabLoginToken: string;
    gitlabHost: string;
    gitlabTargetBranch: string;
}
