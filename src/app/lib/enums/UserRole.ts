export enum UserRole {
  CreateQuiz            = 'CreateQuiz', //
  CreateExpiredQuiz     = 'CreateExpiredQuiz', //
  EditI18n              = 'EditI18n', //
  QuizAdmin             = 'QuizAdmin', //
  SuperAdmin            = 'SuperAdmin', //
  ConfAdmin             = 'ConfAdmin', //
}
